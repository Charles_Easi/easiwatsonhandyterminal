package com.example.easiwatsonhandyterminal.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.easiwatsonhandyterminal.model.Store
import com.example.easiwatsonhandyterminal.repository.StoreRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class StoreViewModel @Inject constructor(private val storeRepository: StoreRepository): ViewModel() {

    private val _response = MutableLiveData<List<Store>>()
    val storeResponse: LiveData<List<Store>> get() = _response

    private val _error = MutableLiveData<String>()
    val errorResponse: LiveData<String> get() = _error

    init {
        getStoreList()
    }

    private fun getStoreList() = viewModelScope.launch {
        try {
            storeRepository.getStoresList().let { response ->
                if (response.isSuccessful) {
                    _response.postValue(response.body()!!.stores)
                } else {
                    val error: String = when {
                        response.body() != null -> {
                            response.body()!!.message
                        }
                        else -> {
                            response.raw().code().toString() + " " + response.raw().message()
                        }
                    }
                    _error.postValue(error)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}