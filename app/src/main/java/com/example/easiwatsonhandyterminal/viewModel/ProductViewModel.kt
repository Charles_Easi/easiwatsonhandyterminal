package com.example.easiwatsonhandyterminal.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.easiwatsonhandyterminal.model.Product
import com.example.easiwatsonhandyterminal.repository.ProductRepository
import com.example.easiwatsonhandyterminal.utils.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class ProductViewModel @Inject constructor(private val productRepository: ProductRepository): ViewModel(){

    private val _response = MutableLiveData<Product?>()
    val productResponse : LiveData<Product?> get() = _response

    private val _error = MutableLiveData<String?>()
    val errorResponse: LiveData<String?> get() = _error

    fun getProductByCode(code: String) = viewModelScope.launch {
        try {
            productRepository.getProductByCode(code).let { response ->
                if (response.isSuccessful) {
                    _response.postValue(response.body())
                } else {
                    val error: String = when {
                        response.body() != null -> {
                            response.body()!!.message
                        }
                        else -> {
                            response.raw().code().toString() + " " + response.raw().message()
                        }
                    }
                    _error.postValue(error)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun clear() {
        _response.value = null
        _error.value = null
    }
}