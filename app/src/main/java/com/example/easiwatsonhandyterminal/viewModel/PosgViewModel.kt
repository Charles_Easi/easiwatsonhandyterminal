package com.example.easiwatsonhandyterminal.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.easiwatsonhandyterminal.model.POSG
import com.example.easiwatsonhandyterminal.model.PosgUploadResponse
import com.example.easiwatsonhandyterminal.repository.PosgRepository
import com.example.easiwatsonhandyterminal.utils.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class PosgViewModel @Inject constructor(private val posgRepository: PosgRepository): ViewModel() {

    private val _response = MutableLiveData<PosgUploadResponse?>()
    val productResponse : LiveData<PosgUploadResponse?> get() = _response

    private val _error = MutableLiveData<String?>()
    val errorResponse: LiveData<String?> get() = _error

    fun uploadPosg(posg: POSG) = viewModelScope.launch {
        try {
            posgRepository.postPOSGUpload(posg).let { response ->
                if (response.isSuccessful) {
                    _response.postValue(response.body())
                } else {
                    val error: String = when {
                        response.body() != null -> {
                            response.body()!!.message
                        }
                        else -> {
                            response.raw().code().toString() + " " + response.raw().message()
                        }
                    }
                    _error.postValue(error)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun clear() {
        _response.value = null
        _error.value = null
    }
}