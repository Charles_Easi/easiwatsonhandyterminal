package com.example.easiwatsonhandyterminal.model

import java.io.Serializable

data class POSG(
    var code: String,
    var storeCode: String,
    var imageFront: String,
    var imageBack: String,
    var imageTop: String,
    var imageBottom: String,
    var imageLeft: String,
    var imageRight: String
): Serializable