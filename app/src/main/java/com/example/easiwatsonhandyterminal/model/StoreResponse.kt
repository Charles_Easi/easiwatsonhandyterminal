package com.example.easiwatsonhandyterminal.model

import java.io.Serializable

data class StoreResponse(
    var stores: List<Store>,
    var error: Boolean,
    var message: String
): Serializable