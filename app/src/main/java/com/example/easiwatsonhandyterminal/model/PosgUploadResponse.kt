package com.example.easiwatsonhandyterminal.model

import java.io.Serializable

data class PosgUploadResponse(
    var error: Boolean,
    var message: String
): Serializable