package com.example.easiwatsonhandyterminal.model

import java.io.Serializable

data class Product(
    var itemCode: String,
    var itemDescription: String,
    var itemLocation: String,
    var itemOrderChannel: String,
    var itemReqStock: Int,
    var itemStock: Int,
    var itemPrice: Double,
    var error: Boolean,
    var message: String
): Serializable