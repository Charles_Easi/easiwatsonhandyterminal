package com.example.easiwatsonhandyterminal.model

import java.io.Serializable

data class Store(
    var storeCode: String,
    var storeDescription: String
): Serializable