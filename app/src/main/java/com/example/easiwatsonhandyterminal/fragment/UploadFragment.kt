package com.example.easiwatsonhandyterminal.fragment

import android.Manifest
import android.content.ContextWrapper
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.easiwatsonhandyterminal.R
import com.example.easiwatsonhandyterminal.activity.MainActivity
import com.example.easiwatsonhandyterminal.databinding.FragmentUploadBinding
import com.example.easiwatsonhandyterminal.dialog.CameraOrGalleryDialog
import com.example.easiwatsonhandyterminal.dialog.ErrorDialog
import com.example.easiwatsonhandyterminal.dialog.SuccessDialog
import com.example.easiwatsonhandyterminal.interfaces.CameraGalleryListener
import com.example.easiwatsonhandyterminal.interfaces.SuccessListener
import com.example.easiwatsonhandyterminal.model.POSG
import com.example.easiwatsonhandyterminal.model.Product
import com.example.easiwatsonhandyterminal.tools.Constant
import com.example.easiwatsonhandyterminal.tools.Preference
import com.example.easiwatsonhandyterminal.utils.ImageUtils
import com.example.easiwatsonhandyterminal.utils.forceHideSoftKeyboard
import com.example.easiwatsonhandyterminal.utils.forceShowSoftKeyboard
import com.example.easiwatsonhandyterminal.viewModel.PosgViewModel
import com.example.easiwatsonhandyterminal.viewModel.ProductViewModel
import com.google.gson.Gson
import com.kaopiz.kprogresshud.KProgressHUD
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UploadFragment : Fragment(), CameraGalleryListener, SuccessListener {

    private var _binding: FragmentUploadBinding?= null
    private val binding get() = _binding!!
    private lateinit var mView: View

    private val productViewModel: ProductViewModel by viewModels()
    private val posgViewModel: PosgViewModel by viewModels()

    private var errorDialog: ErrorDialog?= null
    private var cameraOrGalleryDialog: CameraOrGalleryDialog?= null
    private var successDialog: SuccessDialog?= null

    private var selectedPosition = -1

    private var isAttachedFront = false
    private var isAttachedBack = false
    private var isAttachedTop = false
    private var isAttachedBottom = false
    private var isAttachedLeft = false
    private var isAttachedRight = false

    private var progressDialog: KProgressHUD?= null

    companion object fun newInstance(): UploadFragment {
        return UploadFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentUploadBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mView = view

        initObserver()
        initClickEvent()
        setupToolBarName()
        setFocusableToEditText()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
        posgViewModel.clear()
        productViewModel.clear()
    }

    override fun onCameraSelected(position: Int) {
        selectedPosition = position

        if (ContextCompat.checkSelfPermission(requireContext(),
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), Constant.MY_PERMISSIONS_REQUEST_CAMERA)
        } else {
            openCamera()
        }
    }

    override fun onGallerySelected(position: Int) {
        selectedPosition = position

        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE), Constant.MY_PERMISSION_REQUEST_READ_WRITE)
        } else {
            openGallery()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            Constant.MY_PERMISSIONS_REQUEST_CAMERA -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(context, getString(R.string.action_permission_granted),
                        Toast.LENGTH_SHORT).show()
                    openCamera()
                } else {
                    Toast.makeText(context, getString(R.string.action_permission_denied),
                        Toast.LENGTH_SHORT).show()
                }
            }
            Constant.MY_PERMISSION_REQUEST_READ_WRITE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(context, getString(R.string.action_permission_granted),
                        Toast.LENGTH_SHORT).show()
                    openGallery()
                } else {
                    Toast.makeText(context, getString(R.string.action_permission_denied),
                        Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Constant.OPEN_CAMERA -> {
                if (data != null) {
                    val bitmap = data.extras?.get("data") as Bitmap
                    setImageBasedOnPosition(selectedPosition, bitmap)
                }
            }
            Constant.OPEN_GALLERY -> {
                if (data != null) {
                    val selectedImage: Uri = data.data!!
                    val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

                    val cursor: Cursor = context?.contentResolver?.query(selectedImage,
                        filePathColumn, null, null, null)!!
                    cursor.moveToFirst()

                    val columnIndex: Int = cursor.getColumnIndex(filePathColumn[0])
                    val filePath: String = cursor.getString(columnIndex)
                    cursor.close()

                    val bitmap = BitmapFactory.decodeFile(filePath)
                    if(bitmap != null) {
                        setImageBasedOnPosition(selectedPosition, bitmap)
                    }
                }
            }
        }
    }

    override fun onSuccess() {
        clearAllResultAndImage()
    }

    private fun initObserver() {
        productViewModel.productResponse.observe(viewLifecycleOwner) { product ->
            dismissLoadingProgressBar()

            if (product != null) {
                if (!product.error) {
                    showProductDetail(product)
                    binding.tvErrorItemNotFound.visibility = View.GONE
                } else {
                    //showErrorDialog(product.message)
                    binding.tvErrorItemNotFound.text = product.message
                    binding.tvErrorItemNotFound.visibility = View.VISIBLE
                    clearProductDetail()
                }
            }
            setFocusableToEditText()
        }

        productViewModel.errorResponse.observe(viewLifecycleOwner) { error ->
            dismissLoadingProgressBar()

            if (error != null) {
                showErrorDialog(error)
            }
            setFocusableToEditText()
        }

        posgViewModel.productResponse.observe(viewLifecycleOwner) { response ->
            dismissLoadingProgressBar()

            if (response != null) {
                if (!response.error) {
                    showSaveSuccessDialog()
                } else {
                    showErrorDialog(response.message)
                }
            }
            setFocusableToEditText()
        }

        posgViewModel.errorResponse.observe(viewLifecycleOwner) { error ->
            dismissLoadingProgressBar()

            if (error != null) {
                showErrorDialog(error)
            }
            setFocusableToEditText()
        }
    }

    private fun initClickEvent() {
        binding.rlSearch.setOnClickListener {
            validateProductCode()
        }

        binding.ivFront.setOnClickListener {
            showCameraOrGalleryDialog(0)
        }

        binding.ivBack.setOnClickListener {
            showCameraOrGalleryDialog(1)
        }

        binding.ivTop.setOnClickListener {
            showCameraOrGalleryDialog(2)
        }

        binding.ivBottom.setOnClickListener {
            showCameraOrGalleryDialog(3)
        }

        binding.ivLeft.setOnClickListener {
            showCameraOrGalleryDialog(4)
        }

        binding.ivRight.setOnClickListener {
            showCameraOrGalleryDialog(5)
        }

        binding.btnSave.setOnClickListener {
            uploadImage()
        }

        binding.btnClear.setOnClickListener {
            clearAllResultAndImage()
        }

        binding.btnClose.setOnClickListener {
            fragmentManager?.popBackStack()
            (activity as MainActivity).setSelectedMenuItem(0)
        }

        binding.textInputEditTextProductCode.apply {
            this.setOnEditorActionListener { _, _, _ ->
                validateProductCode()
                false
            }
        }

        binding.textInputEditTextProductCode.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                if (Preference.prefShowKeyboard) {
                    forceShowSoftKeyboard(requireActivity(), requireView())
                } else {
                    binding.textInputEditTextProductCode.showSoftInputOnFocus = false
                }
            }
        }
    }

    private fun uploadImage() {
        val code = binding.tvProductCode.text.toString().trim()

        when {
            code.equals("", true) -> {
                binding.textInputEditTextProductCode.error = getString(R.string.error_field_required)
                return
            }
            !isAttachedFront || !isAttachedBack || !isAttachedTop || !isAttachedBottom ||
                    !isAttachedLeft || !isAttachedRight -> {
                showErrorDialog(getString(R.string.label_upload_all_image))
                return
            }
            else -> {
                forceHideSoftKeyboard(requireActivity(), requireView())

                val encodedFront = ImageUtils().encodeImage(binding.ivFront.drawable.toBitmap())
                val encodedBack = ImageUtils().encodeImage(binding.ivBack.drawable.toBitmap())
                val encodedTop = ImageUtils().encodeImage(binding.ivTop.drawable.toBitmap())
                val encodedBottom = ImageUtils().encodeImage(binding.ivBottom.drawable.toBitmap())
                val encodedLeft = ImageUtils().encodeImage(binding.ivLeft.drawable.toBitmap())
                val encodedRight = ImageUtils().encodeImage(binding.ivRight.drawable.toBitmap())

                val posg = POSG(
                    code = code,
                    storeCode = Preference.prefStoreCode,
                    imageFront = encodedFront,
                    imageBack = encodedBack,
                    imageTop = encodedTop,
                    imageBottom = encodedBottom,
                    imageLeft = encodedLeft,
                    imageRight = encodedRight
                )
//                val gson = Gson()
//                val json = gson.toJson(posg)
//                Log.d("posg", ""+json)
                showLoadingProgressBar()
                posgViewModel.uploadPosg(posg = posg)
            }
        }
    }

    private fun showProductDetail(product: Product) {
        binding.tvProductCode.text = product.itemCode
        binding.tvProductDesc.text = product.itemDescription
    }

    private fun clearProductDetail() {
        binding.tvProductCode.text = ""
        binding.tvProductDesc.text = ""
    }

    private fun clearAllResultAndImage() {
        binding.textInputEditTextProductCode.text!!.clear()
        binding.tvProductCode.text = ""
        binding.tvProductDesc.text = ""
        binding.tvErrorItemNotFound.visibility = View.GONE

        binding.ivFront.setImageResource(R.drawable.ic_upload_photo)
        binding.ivBack.setImageResource(R.drawable.ic_upload_photo)
        binding.ivTop.setImageResource(R.drawable.ic_upload_photo)
        binding.ivBottom.setImageResource(R.drawable.ic_upload_photo)
        binding.ivLeft.setImageResource(R.drawable.ic_upload_photo)
        binding.ivRight.setImageResource(R.drawable.ic_upload_photo)

        isAttachedFront = false
        isAttachedBack = false
        isAttachedTop = false
        isAttachedBottom = false
        isAttachedLeft = false
        isAttachedRight = false
    }

    private fun validateProductCode() {
        val code = binding.textInputEditTextProductCode.text.toString().trim()

        when {
            code.isEmpty() -> {
                //binding.textInputEditTextProductCode.error = getString(R.string.error_field_required)
                return
            }
            else -> {
                forceHideSoftKeyboard(requireActivity(), requireView())
                showLoadingProgressBar()
                productViewModel.getProductByCode(code = code)

                binding.textInputEditTextProductCode.text!!.clear()
                setFocusableToEditText()
            }
        }
    }

    private fun openCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(cameraIntent, Constant.OPEN_CAMERA)
    }

    private fun openGallery() {
        val galleyIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        startActivityForResult(galleyIntent,  Constant.OPEN_GALLERY)
    }

    private fun setImageBasedOnPosition(selectedPosition: Int, bitmap: Bitmap) {
        when (selectedPosition) {
            0 -> {
                setImage(bitmap, binding.ivFront)
                isAttachedFront = true
            }
            1 -> {
                setImage(bitmap, binding.ivBack)
                isAttachedBack = true
            }
            2 -> {
                setImage(bitmap, binding.ivTop)
                isAttachedTop = true
            }
            3 -> {
                setImage(bitmap, binding.ivBottom)
                isAttachedBottom = true
            }
            4 -> {
                setImage(bitmap, binding.ivLeft)
                isAttachedLeft = true
            }
            5 -> {
                setImage(bitmap, binding.ivRight)
                isAttachedRight = true
            }
        }
    }

    private fun setImage(bitmap: Bitmap, image: AppCompatImageView) {
        val resizeBitmap = Bitmap.createScaledBitmap(bitmap, image.width, image.height, true)
        image.setImageBitmap(resizeBitmap)
    }

    private fun showSaveSuccessDialog() {
        successDialog = SuccessDialog(context, this,
            getString(R.string.action_upload_success), getString(R.string.action_image_saved))
        successDialog!!.show()
        successDialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    private fun showCameraOrGalleryDialog(position: Int) {
        cameraOrGalleryDialog = CameraOrGalleryDialog(context, this, position)
        cameraOrGalleryDialog!!.show()
        cameraOrGalleryDialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    private fun showErrorDialog(error: String) {
        errorDialog = ErrorDialog(context, getString(R.string.action_warning), error)
        errorDialog!!.show()
        errorDialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    private fun setFocusableToEditText() {
        binding.textInputEditTextProductCode.isFocusable = true
        binding.textInputEditTextProductCode.requestFocus()
    }

    private fun showLoadingProgressBar() {
        val mContext = (context as ContextWrapper).baseContext

        progressDialog = KProgressHUD.create(mContext)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel(requireContext().getString(R.string.label_please_wait))
            .setCancellable(false)
            .setAnimationSpeed(1)
            .setDimAmount(0.5f)
            .show()
    }

    private fun dismissLoadingProgressBar() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }

    private fun setupToolBarName() {
        (activity as AppCompatActivity?)!!.supportActionBar!!.title =
            getString(R.string.action_pogs_upload)
    }
}