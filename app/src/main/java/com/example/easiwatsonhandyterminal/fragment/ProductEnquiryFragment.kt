package com.example.easiwatsonhandyterminal.fragment

import android.content.ContextWrapper
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.easiwatsonhandyterminal.R
import com.example.easiwatsonhandyterminal.activity.MainActivity
import com.example.easiwatsonhandyterminal.databinding.FragmentProductEnquiryBinding
import com.example.easiwatsonhandyterminal.dialog.ErrorDialog
import com.example.easiwatsonhandyterminal.model.Product
import com.example.easiwatsonhandyterminal.tools.Preference
import com.example.easiwatsonhandyterminal.utils.forceHideSoftKeyboard
import com.example.easiwatsonhandyterminal.utils.forceShowSoftKeyboard
import com.example.easiwatsonhandyterminal.viewModel.ProductViewModel
import com.kaopiz.kprogresshud.KProgressHUD
import dagger.hilt.android.AndroidEntryPoint
import java.text.DecimalFormat

@AndroidEntryPoint
class ProductEnquiryFragment : Fragment() {

    private var _binding: FragmentProductEnquiryBinding?= null
    private val binding get() = _binding!!
    private lateinit var mView: View

    private val productViewModel: ProductViewModel by viewModels()

    private var errorDialog: ErrorDialog?= null

    private val decimalFormat: DecimalFormat = DecimalFormat("####0.00")

    private var progressDialog: KProgressHUD?= null

    companion object fun newInstance(): ProductEnquiryFragment {
        return ProductEnquiryFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentProductEnquiryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mView = view

        initObserver()
        initClickEvent()
        setupToolBarName()
        setFocusableToEditText()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
        productViewModel.clear()
    }

    private fun initObserver() {
        productViewModel.productResponse.observe(viewLifecycleOwner) { product ->
            dismissLoadingProgressBar()

            if (product != null) {
                if (!product.error) {
                    showProductDetail(product)
                } else {
                    showNoItemFound()
                }
            }
            setFocusableToEditText()
        }

        productViewModel.errorResponse.observe(viewLifecycleOwner) { error ->
            dismissLoadingProgressBar()

            if (error != null) {
                showErrorDialog(error)
            }
            setFocusableToEditText()
        }
    }

    private fun initClickEvent() {
        binding.rlSearch.setOnClickListener {
            validateProductCode()
        }

        binding.btnClear.setOnClickListener {
            clearResult()
        }

        binding.btnClose.setOnClickListener {
            fragmentManager?.popBackStack()
            (activity as MainActivity).setSelectedMenuItem(0)
        }

        binding.textInputEditTextProductCode.apply {
            this.setOnEditorActionListener { _, _, _ ->
                validateProductCode()
                false
            }
        }

        binding.textInputEditTextProductCode.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                if (Preference.prefShowKeyboard) {
                    forceShowSoftKeyboard(requireActivity(), requireView())
                } else {
                    binding.textInputEditTextProductCode.showSoftInputOnFocus = false
                }
            }
        }
    }

    private fun validateProductCode() {
        val code = binding.textInputEditTextProductCode.text.toString().trim()

        when {
            code.isEmpty() -> {
                //binding.textInputEditTextProductCode.error = getString(R.string.error_field_required)
                return
            }
            else -> {
                forceHideSoftKeyboard(requireActivity(), requireView())
                showLoadingProgressBar()
                productViewModel.getProductByCode(code = code)

                binding.textInputEditTextProductCode.text!!.clear()
                setFocusableToEditText()
            }
        }
    }

    private fun showProductDetail(product: Product) {
        binding.llProductResult.visibility = View.VISIBLE
        binding.llItemNotFound.visibility = View.GONE

        binding.tvProductCode.text = product.itemCode
        binding.tvProductDesc.text = product.itemDescription

        val itemLocation = product.itemLocation
        if (itemLocation.isNotEmpty()) {
            binding.tvProductLocation.text = itemLocation
        } else {
            binding.tvProductLocation.text = "-"
        }

        val orderChannel = product.itemOrderChannel
        if (orderChannel.isNotEmpty()) {
            binding.tvProductOrderChannel.text = orderChannel
        } else {
            binding.tvProductOrderChannel.text = "-"
        }

        val reqStock = product.itemReqStock.toString()
        binding.tvProductReqStock.text = reqStock

        val stockOnHand = product.itemStock.toString()
        binding.tvProductStockOnHand.text = stockOnHand

        val price = Preference.prefCurrency + " " + decimalFormat.format(product.itemPrice)
        binding.tvProductSalePrice.text = price
    }

    private fun showNoItemFound() {
        binding.llProductResult.visibility = View.GONE
        binding.llItemNotFound.visibility = View.VISIBLE
    }

    private fun clearResult() {
        binding.textInputEditTextProductCode.text!!.clear()

        binding.llProductResult.visibility = View.GONE
        binding.llItemNotFound.visibility = View.GONE
    }

    private fun showErrorDialog(error: String) {
        errorDialog = ErrorDialog(context, getString(R.string.action_warning), error)
        errorDialog!!.show()
        errorDialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    private fun setFocusableToEditText() {
        binding.textInputEditTextProductCode.isFocusable = true
        binding.textInputEditTextProductCode.requestFocus()
    }

    private fun showLoadingProgressBar() {
        val mContext = (context as ContextWrapper).baseContext

        progressDialog = KProgressHUD.create(mContext)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel(requireContext().getString(R.string.label_please_wait))
            .setCancellable(false)
            .setAnimationSpeed(1)
            .setDimAmount(0.5f)
            .show()
    }

    private fun dismissLoadingProgressBar() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }

    private fun setupToolBarName() {
        (activity as AppCompatActivity?)!!.supportActionBar!!.title =
            getString(R.string.action_product_enquiry)
    }
}