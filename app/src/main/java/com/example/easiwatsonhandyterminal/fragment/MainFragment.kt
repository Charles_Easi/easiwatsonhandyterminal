package com.example.easiwatsonhandyterminal.fragment

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.easiwatsonhandyterminal.R
import com.example.easiwatsonhandyterminal.activity.MainActivity
import com.example.easiwatsonhandyterminal.databinding.FragmentMainBinding
import com.example.easiwatsonhandyterminal.dialog.SettingDialog
import com.example.easiwatsonhandyterminal.interfaces.SetupUrlDialogListener
import com.example.easiwatsonhandyterminal.tools.Constant
import com.example.easiwatsonhandyterminal.tools.Preference
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : Fragment(), SetupUrlDialogListener {

    private var _binding: FragmentMainBinding?= null
    private val binding get() = _binding!!
    private lateinit var mView: View

    private var settingDialog: SettingDialog?= null

    companion object fun newInstance(): MainFragment {
        return MainFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mView = view

        setupToolBarName()
        checkingForBaseUrl()
        initClickEvent()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onSetupClick() {
        goToSettingFragment()
    }

    private fun checkingForBaseUrl() {
        if (Preference.prefWebServiceUrl.equals("", ignoreCase = true)){
            binding.llLocationOutlet.visibility = View.GONE
            showGoToSettingDialog()
        } else {
            if (!Preference.prefStoreDesc.equals("", ignoreCase = true)) {
                binding.llLocationOutlet.visibility = View.VISIBLE
                binding.tvOutlet.text = Preference.prefStoreDesc
            }
        }
    }

    private fun initClickEvent() {
        binding.llProductEnquiry.setOnClickListener {
            goToProductEnquiryFragment()
        }

        binding.llPogsUpload.setOnClickListener {
            goToUploadFragment()
        }

        binding.llSetting.setOnClickListener {
           goToSettingFragment()
        }
    }

    private fun showGoToSettingDialog() {
        settingDialog = SettingDialog(context, this)
        settingDialog!!.show()
        settingDialog!!.setCancelable(false)
        settingDialog!!.setCanceledOnTouchOutside(false)
        settingDialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    private fun goToSettingFragment() {
        val settingFragment = SettingFragment().newInstance()
        replaceFragment(settingFragment, Constant.TAG_SETTING)
        (activity as MainActivity).setSelectedMenuItem(3)
    }

    private fun goToUploadFragment() {
        val uploadFragment = UploadFragment().newInstance()
        replaceFragment(uploadFragment, Constant.TAG_UPLOAD)
        (activity as MainActivity).setSelectedMenuItem(2)
    }

    private fun goToProductEnquiryFragment() {
        val productEnquiryFragment = ProductEnquiryFragment().newInstance()
        replaceFragment(productEnquiryFragment, Constant.TAG_PRODUCT_ENQUIRY)
        (activity as MainActivity).setSelectedMenuItem(1)
    }

    private fun replaceFragment(fragment: Fragment, tag: String) {
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(R.id.nav_host_fragment, fragment, tag)
        transaction.addToBackStack(Constant.TAG_MAIN)
        transaction.commitAllowingStateLoss()
    }

    private fun setupToolBarName() {
        (activity as AppCompatActivity?)!!.supportActionBar!!.title =
            getString(R.string.action_main)
    }
}