package com.example.easiwatsonhandyterminal.fragment

import android.annotation.SuppressLint
import android.content.ContextWrapper
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.easiwatsonhandyterminal.BuildConfig
import com.example.easiwatsonhandyterminal.R
import com.example.easiwatsonhandyterminal.activity.MainActivity
import com.example.easiwatsonhandyterminal.databinding.FragmentSettingBinding
import com.example.easiwatsonhandyterminal.dialog.ErrorDialog
import com.example.easiwatsonhandyterminal.dialog.SuccessDialog
import com.example.easiwatsonhandyterminal.dialog.WarningDialog
import com.example.easiwatsonhandyterminal.interfaces.SuccessListener
import com.example.easiwatsonhandyterminal.interfaces.WarningDialogListener
import com.example.easiwatsonhandyterminal.model.Store
import com.example.easiwatsonhandyterminal.tools.Preference
import com.example.easiwatsonhandyterminal.utils.CurrencyUtils
import com.example.easiwatsonhandyterminal.viewModel.StoreViewModel
import com.kaopiz.kprogresshud.KProgressHUD
import dagger.hilt.android.AndroidEntryPoint
import kotlin.collections.ArrayList

@AndroidEntryPoint
class SettingFragment : Fragment(), WarningDialogListener, SuccessListener {

    private var _binding: FragmentSettingBinding?= null
    private val binding get() = _binding!!
    private lateinit var mView: View

    private val storeViewModel: StoreViewModel by viewModels()

    private var currencyList: List<String> = ArrayList()
    private var selectedCurrency: String?= null
    private var selectedStoreCode: String?= null
    private var selectedStoreDesc: String?= null
    private var selectedCurrencyPos = -1
    private var selectedStorePos = -1

    private val storeCodeList: MutableList<String> = ArrayList()
    private val storeDescList: MutableList<String> = ArrayList()

    private var warningDialog: WarningDialog?= null
    private var errorDialog: ErrorDialog?= null
    private var successDialog: SuccessDialog?= null
    private var progressDialog: KProgressHUD?= null

    companion object fun newInstance(): SettingFragment {
        return SettingFragment()
    }

    init {
        currencyList = CurrencyUtils().getAllCurrencies()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentSettingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mView = view

        initObserver()
        initClickEvent()
        setupToolBarName()
        initCurrencySpinnerData()
        initUrlAndCurrencyPrefData()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onWarningDialogClicked() {
        showLoadingProgressBar()
        enableSpinner()
        initUrlAndCurrencyPrefData()
        binding.textInputLayoutWebServiceUrl.isEnabled = false
        initObserver()
    }

    override fun onSuccess() {
        fragmentManager?.popBackStack()
        (activity as MainActivity).setSelectedMenuItem(0)
    }

    private fun initStoreSpinnerData(storeList: List<Store>) {
        enableSpinner()

        if (storeList.isNotEmpty()) {
            for (store in storeList) {
                storeCodeList.add(store.storeCode)
                storeDescList.add(store.storeDescription)
            }
        }

        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(requireContext(),
            R.layout.spinner_item_layout, storeDescList.toList())
        binding.spinnerStore.adapter = adapter

        initStorePrefData()
    }

    private fun initCurrencySpinnerData() {
        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(requireContext(),
            R.layout.spinner_item_layout, currencyList)
        binding.spinnerCurrency.adapter = adapter
    }

    private fun initObserver() {
        if (!Preference.prefWebServiceUrl.equals("", ignoreCase = true)) {
            storeViewModel.storeResponse.observe(viewLifecycleOwner) { storeList ->
                dismissLoadingProgressBar()
                initStoreSpinnerData(storeList)
            }
            storeViewModel.errorResponse.observe(viewLifecycleOwner) { error ->
                dismissLoadingProgressBar()
                showErrorDialog(error)
            }
        } else {
            disableSpinner()
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initClickEvent() {
        binding.btnSave.setOnClickListener {
            validateAndSaveUrl()
        }

        binding.btnClose.setOnClickListener {
            fragmentManager?.popBackStack()
            (activity as MainActivity).setSelectedMenuItem(0)
        }

        binding.spinnerCurrency.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {
                selectedCurrencyPos = pos
                selectedCurrency = currencyList[pos]
                binding.spinnerCurrency.setSelection(pos)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }

        binding.spinnerStore.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {
                selectedStorePos = pos
                selectedStoreCode = storeCodeList[pos]
                selectedStoreDesc = storeDescList[pos]
                binding.spinnerStore.setSelection(pos)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }
        }

        binding.switchShowSoftKeyboard.setOnCheckedChangeListener { _, state ->
            Preference.prefShowKeyboard = state
            binding.switchShowSoftKeyboard.isChecked = state
        }
    }

    private fun initUrlAndCurrencyPrefData() {
        if (!Preference.prefWebServiceUrl.equals("", ignoreCase = true)) {
            val url = Preference.prefWebServiceUrl.trim()
            binding.textInputEditTextWebServiceUrl.setText(url)
        }
        if (Preference.prefCurrencyPos != -1 ) {
            val currencyPos = Preference.prefCurrencyPos
            binding.spinnerCurrency.setSelection(currencyPos)
        }
        //show switch state
        binding.switchShowSoftKeyboard.isChecked = Preference.prefShowKeyboard
    }

    private fun initStorePrefData() {
        if (Preference.prefStorePos != -1) {
            val storePos = Preference.prefStorePos
            binding.spinnerStore.setSelection(storePos)
        }
    }

    private fun validateAndSaveUrl() {
        var baseUrl = binding.textInputEditTextWebServiceUrl.text.toString().trim()
        val isUrlValid = Patterns.WEB_URL.matcher(baseUrl).matches()

        when {
            baseUrl.isEmpty() -> {
                binding.textInputLayoutWebServiceUrl.error = getString(R.string.error_field_required)
                return
            }
            isUrlValid -> {
                if (baseUrl.endsWith("/").not()) {
                    baseUrl += "/"
                }
            }
            else -> {
                binding.textInputLayoutWebServiceUrl.error = getString(R.string.error_invalid_url)
                return
            }
        }
        //store baseURL
        Preference.prefWebServiceUrl = baseUrl

        if (selectedCurrency != null && selectedStoreDesc != null && selectedStoreCode != null) {
            //Store currency & currencyPos
            Preference.prefCurrency = selectedCurrency!!
            Preference.prefCurrencyPos = selectedCurrencyPos

            //Store storeCode, desc, pos
            Preference.prefStoreDesc = selectedStoreDesc!!
            Preference.prefStoreCode = selectedStoreCode!!
            Preference.prefStorePos = selectedStorePos
        }

        if (Preference.prefCurrency.equals("", ignoreCase = true) ||
            Preference.prefStoreDesc.equals("", ignoreCase = true)) {
            showWarningDialog()
        } else {
            showSaveSuccessDialog()
        }
    }

    private fun showSaveSuccessDialog() {
        successDialog = SuccessDialog(context, this,
            getString(R.string.action_save_success), getString(R.string.action_setting_saved))
        successDialog!!.show()
        successDialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    private fun showErrorDialog(error: String) {
        errorDialog = ErrorDialog(context, getString(R.string.action_warning), error)
        errorDialog!!.show()
        errorDialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    private fun showWarningDialog() {
        warningDialog = WarningDialog(context, this,
            getString(R.string.label_url_saved), getString(R.string.action_setup_warn))
        warningDialog!!.show()
        warningDialog!!.setCancelable(false)
        warningDialog!!.setCanceledOnTouchOutside(false)
        warningDialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    private fun setupToolBarName() {
        (activity as AppCompatActivity?)!!.supportActionBar!!.title =
            getString(R.string.action_setting)

        binding.tvVersion.text = BuildConfig.VERSION_NAME
    }

    private fun enableSpinner() {
        binding.spinnerStore.isEnabled = true
        binding.spinnerCurrency.isEnabled = true
    }

    private fun showLoadingProgressBar() {
        val mContext = (context as ContextWrapper).baseContext

        progressDialog = KProgressHUD.create(mContext)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel(requireContext().getString(R.string.label_please_wait))
            .setCancellable(false)
            .setAnimationSpeed(1)
            .setDimAmount(0.5f)
            .show()
    }

    private fun dismissLoadingProgressBar() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }

    private fun disableSpinner() {
        binding.spinnerStore.isEnabled = false
        binding.spinnerCurrency.isEnabled = false
    }
}