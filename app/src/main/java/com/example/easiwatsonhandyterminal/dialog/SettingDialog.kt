package com.example.easiwatsonhandyterminal.dialog

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import com.example.easiwatsonhandyterminal.databinding.DialogSettingBinding
import com.example.easiwatsonhandyterminal.interfaces.SetupUrlDialogListener

class SettingDialog(context: Context?, private val listener: SetupUrlDialogListener): AlertDialog(context) {

    private var _binding: DialogSettingBinding?= null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        _binding = DialogSettingBinding.inflate(LayoutInflater.from(context))
        setView(binding.root)
        super.onCreate(savedInstanceState)

        initClickEvent()
    }

    private fun initClickEvent() {
        binding.buttonSetup.setOnClickListener {
            listener.onSetupClick()
            dismiss()
        }
    }
}