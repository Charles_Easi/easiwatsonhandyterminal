package com.example.easiwatsonhandyterminal.dialog

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import com.example.easiwatsonhandyterminal.databinding.DialogCameraGalleryBinding
import com.example.easiwatsonhandyterminal.interfaces.CameraGalleryListener

class CameraOrGalleryDialog(context: Context?, private val listener: CameraGalleryListener,
                            private val position: Int): AlertDialog(context) {

    private var _binding: DialogCameraGalleryBinding?= null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        _binding = DialogCameraGalleryBinding.inflate(LayoutInflater.from(context))
        setView(binding.root)
        super.onCreate(savedInstanceState)

        initClickEvent()
    }

    private fun initClickEvent() {
        binding.llOpenCamera.setOnClickListener {
            listener.onCameraSelected(position)
            dismiss()
        }

        binding.llUploadPic.setOnClickListener {
            listener.onGallerySelected(position)
            dismiss()
        }
    }
}