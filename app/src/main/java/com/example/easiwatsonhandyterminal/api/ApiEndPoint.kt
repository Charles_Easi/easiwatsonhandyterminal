package com.example.easiwatsonhandyterminal.api

object ApiEndPoint {
    const val storeEnquiry = "storeEnquiry"
    const val productEnquiry = "productEnquiry"
    const val uploadPOGS = "uploadPOGS"
}

