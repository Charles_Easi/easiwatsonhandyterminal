package com.example.easiwatsonhandyterminal.api

import com.example.easiwatsonhandyterminal.model.POSG
import com.example.easiwatsonhandyterminal.model.PosgUploadResponse
import com.example.easiwatsonhandyterminal.model.Product
import com.example.easiwatsonhandyterminal.model.StoreResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface Api {

    @GET(ApiEndPoint.storeEnquiry)
    suspend fun getStores(): Response<StoreResponse>

    @GET(ApiEndPoint.productEnquiry)
    suspend fun getProductByCode(@Query("code") code: String): Response<Product>

    @POST(ApiEndPoint.uploadPOGS)
    suspend fun postPOSGUpload(@Body posg: POSG): Response<PosgUploadResponse>
}