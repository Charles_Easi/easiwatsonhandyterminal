package com.example.easiwatsonhandyterminal.tools

import com.pixplicity.easyprefs.library.Prefs

object Preference {

    private const val PREF_WEB_SERVICE_URL = "PREF_WEB_SERVICE_URL"
    private const val PREF_CURRENCY = "PREF_CURRENCY"
    private const val PREF_CURRENCY_POS = "PREF_CURRENCY_POS"
    private const val PREF_STORE_CODE = "PREF_STORE_CODE"
    private const val PREF_STORE_DESC = "PREF_STORE_DESC"
    private const val PREF_STORE_POS = "PREF_STORE_POS"
    private const val PREF_SHOW_KEYBOARD = "PREF_SHOW_KEYBOARD"

    var prefWebServiceUrl: String
        get() = Prefs.getString(PREF_WEB_SERVICE_URL, "")
        set(url) = Prefs.putString(PREF_WEB_SERVICE_URL, url)

    var prefCurrency: String
        get() = Prefs.getString(PREF_CURRENCY, "")
        set(currency) = Prefs.putString(PREF_CURRENCY, currency)

    var prefCurrencyPos: Int
        get() = Prefs.getInt(PREF_CURRENCY_POS, -1)
        set(pos) = Prefs.putInt(PREF_CURRENCY_POS, pos)

    var prefStoreCode: String
        get() = Prefs.getString(PREF_STORE_CODE, "")
        set(store) = Prefs.putString(PREF_STORE_CODE, store)

    var prefStoreDesc: String
        get() = Prefs.getString(PREF_STORE_DESC, "")
        set(store) = Prefs.putString(PREF_STORE_DESC, store)

    var prefStorePos: Int
        get() = Prefs.getInt(PREF_STORE_POS, -1)
        set(pos) = Prefs.putInt(PREF_STORE_POS, pos)

    var prefShowKeyboard: Boolean
        get() = Prefs.getBoolean(PREF_SHOW_KEYBOARD, false)
        set(showKeyboard) = Prefs.putBoolean(PREF_SHOW_KEYBOARD, showKeyboard)
}