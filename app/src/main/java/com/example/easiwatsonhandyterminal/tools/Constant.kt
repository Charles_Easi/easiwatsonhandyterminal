package com.example.easiwatsonhandyterminal.tools

object Constant {
    const val MY_PERMISSIONS_REQUEST_CAMERA = 200
    const val OPEN_CAMERA = 201

    const val MY_PERMISSION_REQUEST_READ_WRITE = 300
    const val OPEN_GALLERY = 301

    const val TAG_MAIN = "FRAGMENT_MAIN"
    const val TAG_SETTING = "FRAGMENT_SETTING"
    const val TAG_UPLOAD = "FRAGMENT_UPLOAD"
    const val TAG_PRODUCT_ENQUIRY = "FRAGMENT_PRODUCT_ENQUIRY"
}