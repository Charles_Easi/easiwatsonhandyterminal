package com.example.easiwatsonhandyterminal.activity

import android.content.res.Configuration
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.navigation.ActivityNavigator
import com.example.easiwatsonhandyterminal.R
import com.example.easiwatsonhandyterminal.databinding.ActivityMainBinding
import com.example.easiwatsonhandyterminal.fragment.MainFragment
import com.example.easiwatsonhandyterminal.fragment.ProductEnquiryFragment
import com.example.easiwatsonhandyterminal.fragment.SettingFragment
import com.example.easiwatsonhandyterminal.fragment.UploadFragment
import com.example.easiwatsonhandyterminal.tools.Constant
import com.example.easiwatsonhandyterminal.utils.forceHideSoftKeyboard
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupViews()
        setupListeners()
    }

    private fun setupViews() {
        setSupportActionBar(binding.toolbar)
        initFragment()

        //this prevent menu item having bug
        binding.navMain.itemIconTintList = null
        //set initial checked drawer
        binding.navMain.menu.getItem(0).isChecked = true
        binding.drawerMain.addDrawerListener(toggle)
    }

    private fun initFragment() {
        val mainFragment = MainFragment().newInstance()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.nav_host_fragment, mainFragment, Constant.TAG_MAIN)
        fragmentTransaction.commit()
    }

    private fun setupListeners() {
        binding.navMain.setNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.mainFragment -> {
                    val fragment = supportFragmentManager.fragments.last()
                    if (fragment.tag != Constant.TAG_MAIN) {
                        initFragment()
                        clearAllBackStack()
                    }
                }
                R.id.productEnquiryFragment -> {
                    val fragment = supportFragmentManager.fragments.last()
                    if (fragment.tag != Constant.TAG_PRODUCT_ENQUIRY) {
                        val productEnquiryFragment = ProductEnquiryFragment().newInstance()
                        replaceFragment(productEnquiryFragment, Constant.TAG_PRODUCT_ENQUIRY)
                    }
                }
                R.id.uploadFragment -> {
                    val fragment = supportFragmentManager.fragments.last()
                    if (fragment.tag != Constant.TAG_UPLOAD) {
                        val uploadFragment = UploadFragment().newInstance()
                        replaceFragment(uploadFragment, Constant.TAG_UPLOAD)
                    }
                }
                R.id.settingFragment -> {
                    val fragment = supportFragmentManager.fragments.last()
                    if (fragment.tag != Constant.TAG_SETTING) {
                        val settingFragment = SettingFragment().newInstance()
                        replaceFragment(settingFragment, Constant.TAG_SETTING)
                    }
                }
            }
            closeDrawer()
            forceHideSoftKeyboard(this@MainActivity, binding.drawerMain)
            true
        }
    }

    private val toggle by lazy {
        object: ActionBarDrawerToggle(this, binding.drawerMain, binding.toolbar,
            R.string.action_nav_open, R.string.action_nav_close) {
            override fun onDrawerStateChanged(newState: Int) {
                super.onDrawerStateChanged(newState)
                if (newState == DrawerLayout.STATE_DRAGGING) {
                    forceHideSoftKeyboard(this@MainActivity, binding.drawerMain)
                }
            }

            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                forceHideSoftKeyboard(this@MainActivity, binding.drawerMain)
            }
        }
    }

    override fun finish() {
        super.finish()
        ActivityNavigator.applyPopAnimationsToPendingTransition(this)
    }

    override fun onBackPressed() {
        if (binding.drawerMain.isDrawerOpen(GravityCompat.START)) {
            binding.drawerMain.closeDrawer(GravityCompat.START)
        } else {
            binding.navMain.menu.getItem(0).isChecked = true
            super.onBackPressed()
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        toggle.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        toggle.onConfigurationChanged(newConfig)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun replaceFragment(fragment: Fragment, tag: String) {
        val transaction = supportFragmentManager.beginTransaction()
        supportFragmentManager.popBackStack()
        transaction.replace(R.id.nav_host_fragment, fragment, tag)
        transaction.addToBackStack(Constant.TAG_MAIN)
        transaction.commitAllowingStateLoss()
    }

    private fun clearAllBackStack() {
        val fm = supportFragmentManager
        for (i in 0 until fm.backStackEntryCount) {
            fm.popBackStack()
        }
    }

    private fun closeDrawer() {
        if (binding.drawerMain.isDrawerOpen(GravityCompat.START)) {
            binding.drawerMain.closeDrawer(GravityCompat.START)
        }
    }

    fun setSelectedMenuItem(i: Int) {
        binding.navMain.menu.getItem(i).isChecked = true
    }
}