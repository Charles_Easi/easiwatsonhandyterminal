package com.example.easiwatsonhandyterminal.interfaces

interface ErrorMessageListener {
    fun onError(errorMsg: String?)
}