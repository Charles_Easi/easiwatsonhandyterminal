package com.example.easiwatsonhandyterminal.interfaces

interface SetupUrlDialogListener {
    fun onSetupClick()
}