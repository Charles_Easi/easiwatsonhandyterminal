package com.example.easiwatsonhandyterminal.interfaces

interface SuccessListener {
    fun onSuccess()
}