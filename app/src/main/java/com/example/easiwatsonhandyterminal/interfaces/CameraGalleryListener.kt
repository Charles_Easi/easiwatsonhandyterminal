package com.example.easiwatsonhandyterminal.interfaces

interface CameraGalleryListener {
    fun onCameraSelected(position: Int)

    fun onGallerySelected(position: Int)
}