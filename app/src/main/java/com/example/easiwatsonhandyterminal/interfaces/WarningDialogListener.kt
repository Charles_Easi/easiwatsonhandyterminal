package com.example.easiwatsonhandyterminal.interfaces

interface WarningDialogListener {
    fun onWarningDialogClicked()
}