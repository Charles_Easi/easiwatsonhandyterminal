package com.example.easiwatsonhandyterminal.repository

import com.example.easiwatsonhandyterminal.api.Api
import com.example.easiwatsonhandyterminal.model.POSG
import javax.inject.Inject

class PosgRepository @Inject constructor(private val api: Api)  {

    suspend fun postPOSGUpload(posg: POSG) = api.postPOSGUpload(posg)
}