package com.example.easiwatsonhandyterminal.repository

import com.example.easiwatsonhandyterminal.api.Api
import javax.inject.Inject

class StoreRepository @Inject constructor(private val api: Api) {

    suspend fun getStoresList() = api.getStores()
}