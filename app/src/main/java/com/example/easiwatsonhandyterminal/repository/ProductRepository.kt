package com.example.easiwatsonhandyterminal.repository

import com.example.easiwatsonhandyterminal.api.Api
import javax.inject.Inject

class ProductRepository @Inject constructor(private val api: Api) {

    suspend fun getProductByCode(code: String) = api.getProductByCode(code)
}