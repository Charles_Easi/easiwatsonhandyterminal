package com.example.easiwatsonhandyterminal.utils

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.IdRes
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.customview.widget.Openable
import androidx.navigation.NavController
import androidx.navigation.NavController.OnDestinationChangedListener
import androidx.navigation.NavDestination
import androidx.navigation.ui.NavigationUI
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.navigation.NavigationView
import java.lang.ref.WeakReference

// Custom implementation based on NavigationUI.setupWithNavController
fun NavigationView.setupWithNavController(navController: NavController, block: (MenuItem) -> Unit) {
    this.setNavigationItemSelectedListener { item ->
        block(item)
        val handled = NavigationUI.onNavDestinationSelected(item, navController)
        if (handled) {
            val parent = this.parent
            if (parent is Openable) {
                (parent as Openable).close()
            } else {
                val bottomSheetBehavior =
                    findBottomSheetBehavior(this)
                if (bottomSheetBehavior != null) {
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                }
            }
        }
        handled
    }
    val weakReference = WeakReference(this)
    navController.addOnDestinationChangedListener(
        object : OnDestinationChangedListener {
            override fun onDestinationChanged(controller: NavController, destination: NavDestination, arguments: Bundle?) {
                val view = weakReference.get()
                if (view == null) {
                    navController.removeOnDestinationChangedListener(this)
                    return
                }
                val menu = view.menu
                var h = 0
                val size = menu.size()
                while (h < size) {
                    val item = menu.getItem(h)
                    item.isChecked = matchDestination(destination, item.itemId)
                    h++
                }
            }
        })
}

fun matchDestination(destination: NavDestination, @IdRes destId: Int): Boolean {
    var currentDestination: NavDestination? = destination
    while (currentDestination!!.id != destId && currentDestination.parent != null) {
        currentDestination = currentDestination.parent
    }
    return currentDestination.id == destId
}

fun findBottomSheetBehavior(view: View): BottomSheetBehavior<*>? {
    val params = view.layoutParams
    if (params !is CoordinatorLayout.LayoutParams) {
        val parent = view.parent
        return if (parent is View) {
            findBottomSheetBehavior(parent as View)
        } else null
    }
    val behavior = params
        .behavior
    return if (behavior !is BottomSheetBehavior<*>) {
        // We hit a CoordinatorLayout, but the View doesn't have the BottomSheetBehavior
        null
    } else behavior
}

fun String?.orDash(): String {
    return if (this != null && this.isNotBlank()) {
        this
    } else {
        "-"
    }
}

fun forceHideSoftKeyboard(activity: Activity, view: View) {
    val imm: InputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun forceShowSoftKeyboard(activity: Activity, view: View) {
    view.requestFocus()
    val imm: InputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
}