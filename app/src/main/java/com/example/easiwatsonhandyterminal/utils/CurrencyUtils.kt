package com.example.easiwatsonhandyterminal.utils

import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

class CurrencyUtils {

    fun getAllCurrencies(): List<String> {
        val name: MutableSet<Currency> = HashSet()
        val currencyList: MutableList<String> = ArrayList()
        val locale: Array<Locale> = Locale.getAvailableLocales()
        for (loc in locale) {
            try {
                val currency: Currency = Currency.getInstance(loc)
                name.add(currency)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        for (item in name) {
            currencyList.add(item.currencyCode)
        }
        currencyList.sort()
        //remove currency with name = SGD
        for (i in 0.. currencyList.size) {
            if (currencyList[i].equals("SGD", ignoreCase = true)) {
                currencyList.removeAt(i)
                //add SGD at top on list
                currencyList.add(index = 0, element = "SGD")
                break
            }
        }
        return currencyList
    }
}